import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
  FlatList,
  Modal,
} from 'react-native';
import {BASE_URL, TOKEN} from './url';
import EditDataModal from './EditDataModal';

export default function Detail({navigation, route, show, onClose, onClick}) {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  const {mobileName, mobileKM, mobilePrice, mobileHeader} = route.params;
  var dataMobil = route?.params;

  const [showEditModal, setShowEditModal] = useState(false);

  const bukaEditModal = () => {
    setShowEditModal(true);
  };

  const tutupEditModal = () => {
    setShowEditModal(!showEditModal);
  };

  useEffect(() => {
    if (dataMobil) {
      setNamaMobil(dataMobil.title || ''); // Set default values or handle missing properties
      setTotalKM(dataMobil.totalKM || '');
      setHargaMobil(dataMobil.harga || '');
    }
  }, []);

  const deleteData = async () => {
    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];
    try {
      const response = await fetch(`${BASE_URL}mobil`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          Authorization: TOKEN,
        },
        body: JSON.stringify(body),
      });

      const result = await response.json();
      alert('Data Mobil berhasil dihapus');
      navigation.navigate('Home');
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView
        nestedScrollEnabled
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{paddingBottom: 10}}>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
          keyboardVerticalOffset={-500}>
          <ImageBackground
            source={{uri: mobileHeader}}
            style={{
              width: Dimensions.get('window').width, //atur agar lebar gambar adalah selebar layar device
              height: 317,
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <TouchableOpacity onPress={() => navigation.navigate('Home')}>
                <Image
                  source={require('../assets/icon/back.png')}
                  style={styles.iconAtas}></Image>
              </TouchableOpacity>
            </View>
          </ImageBackground>
          <View style={styles.isiKontenPutih}>
            <Text style={{color: 'black', fontWeight: 'bold', fontSize: 19}}>
              {mobileName}
            </Text>
          </View>
          <View style={styles.border}></View>
          <View style={styles.isiKontenPutih2}>
            <View>
              <Text style={styles.judulDesc}>Total KM</Text>
              <Text style={styles.dalamDesc}>
                Total KM dari mobil ini adalah : {mobileKM} KM
              </Text>
            </View>
            <View style={{marginBottom: 10}}>
              <Text style={styles.judulDesc}>Harga</Text>
              <Text style={styles.dalamDesc}>
                Harga dari mobil ini adalah : {mobilePrice}
              </Text>
            </View>
            <TouchableOpacity
              // onPress={() => editData()}
              onPress={() => bukaEditModal()}
              style={styles.repairButton}>
              <Text
                style={{
                  color: '#fff',
                  fontSize: 16.3,
                  fontWeight: 'bold',
                }}>
                Edit
              </Text>
            </TouchableOpacity>
            {dataMobil ? (
              <TouchableOpacity
                onPress={() => deleteData()}
                style={styles.repairButton}>
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 16.3,
                    fontWeight: 'bold',
                  }}>
                  Delete
                </Text>
              </TouchableOpacity>
            ) : null}
            <EditDataModal
              show={showEditModal}
              onClose={() => tutupEditModal()}
            />
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  isiKontenPutih: {
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 19,
    borderTopRightRadius: 19,
    paddingHorizontal: 33,
    paddingTop: 25,
    marginTop: -20,
  },
  isiKontenPutih2: {
    width: '100%',
    paddingHorizontal: 33,
  },
  judulDesc: {
    color: '#201F26',
    fontSize: 16,
    marginTop: 23,
  },
  dalamDesc: {
    color: '#595959',
    fontSize: 14,
    marginTop: 6,
  },
  border: {
    borderColor: '#EEE',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    marginTop: 13,
  },
  repairButton: {
    width: '100%',
    marginTop: 10,
    backgroundColor: '#BB2427',
    borderRadius: 8,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconAtas: {
    width: 24,
    height: 24,
    marginHorizontal: 26,
    marginTop: 28,
  },
});
